import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sda-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss']
})
export class OneComponent {

  stringVariable: string = 'this is amazing!';
  numberVariable: number = 42;
  booleanVariable: boolean = true;
  arrayOfStringsI: string[] = ['one', 'two', 'three'];
  arrayOfStringsII: Array<string> = ['four', 'five', 'six'];
  variableAsNull: null = null;
  variableAsUndefined: undefined = undefined;
  differentStringInit: string = `I can use a string from variable ${this.stringVariable}`;

  someVariable: any;
  someOtherVariable: unknown;
  arrayOfUnknown: unknown[] = [1, 'two', true];
  arrayOfVariousTypes: Array<number | string | boolean> = [1, 'two', true];
  objectVariable: object = {name: 'John', age: 42}; // try to avoid using object types
  arrayOfObjects: Array<{name: string, age: number}> = [{name: 'John', age: 42}, {name: 'Jane', age: 43}];
  arrayOfObjectsWIthInterface: Array<Person> = [{name: 'Mark', age: 66}, {name: 'Ann', age: 55}];
  variableFromFunction: number = this.addTwoNumbers(6, 2);

  mockString: string = 'Jarek';
  suffixI: string = this.addSuffixToString(this.mockString, 'I');
  suffixII: string = this.addSuffixToString(this.mockString, 2);

  variableFromEnum = Direction.Down;

  private variableThatCannotBeReached = 'this variable cannot be reached';

  addTwoNumbers(num1: number, num2: number): number {
    return num1 + num2;
  }

  addSuffixToString(baseString: string, suffix: string | number): string {
    const base = baseString;
    let addOn = suffix;
    if (typeof addOn === 'number') {
    addOn = ` ${addOn}`;
    }
    return base + addOn;
  }

}


interface Person {
  name: string;
  age: number;
}

enum Direction {
  Up = 'UP',
  Down = 'DOWN',
  Left  = 'LEFT',
  Right = 'RIGHT'
}

class Audio {
  title: string;
  artist: string;
  constructor(title: string, artist: string) {
    this.title = title;
    this.artist = artist;
  }
}
  class Video {
    title: string;
    director: string;
    constructor(title: string, director: string) {
      this.title = title;
      this.director = director;
    }
  }
    class Post<T> {
      content: T;
    }

    let audioPost = new Post<Audio>();
    let videoPost = new Post<Video>();