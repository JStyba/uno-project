import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[sdaColorChangeHover]'
})
export class ColorChangeHoverDirective {

  constructor(private ref: ElementRef, private renderer: Renderer2) { }

@HostListener('mouseenter') onMouseEnter() {
  this.renderer.setStyle(this.ref.nativeElement, 'background-color', 'gray');
}

@HostListener('mouseleave') onMouseLeave() {
  this.renderer.removeStyle(this.ref.nativeElement, 'background-color');
}

}
