import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sda-four',
  templateUrl: './four.component.html',
  styleUrls: ['./four.component.scss']
})
export class FourComponent implements OnInit {
bulb = '../assets/bulb-off.png' // TS known what type this is and we don't need to declare it.
label = 'on'; // TS known what type this is and we don't need to declare it.
show = true; // TS known what type this is and we don't need to declare it.
  constructor() { }

  ngOnInit(): void {
    console.log('bulb', typeof this.bulb)
    console.log('label', typeof this.label)
    console.log('show', typeof this.show)
  }

  changeLight(): void {
    if (this.bulb === '../assets/bulb-off.png') {
      this.show = true;
      this.label = 'off';
      this.bulb = '../assets/bulb-on.png';
    } else {
      this.show = false;
      this.label = 'on';
      this.bulb = '../assets/bulb-off.png';
    }
  }

}
