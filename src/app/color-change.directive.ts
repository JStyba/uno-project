import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[sdaColorChange]'
})
export class ColorChangeDirective {
@Input() sdaColorChange: boolean;
  constructor(private ref: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit(): void {
    this.setColor();
  }


  setColor(): void {
    if (this.sdaColorChange) {
      this.renderer.setStyle(this.ref.nativeElement, 'background-color', '#ededed');
    }
  }
   }


