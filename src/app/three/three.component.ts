import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'sda-three',
  templateUrl: './three.component.html',
  styleUrls: ['./three.component.scss']
})
export class ThreeComponent implements OnInit {
  @ViewChild('input', {static: true}) child: ElementRef;;
  superhero: string = 'Hulk';
  fontSizeFromTS: string = '20px';
  show: boolean = true;
  enable: boolean = true;
  backgroundActive: boolean = true;
  backgrounds = BACKGROUNDS;
  constructor() { }

  ngOnInit(): void {
    console.log(this.child)
  }

}

enum BACKGROUNDS {
  Hulk = 'green',
  Ironman = 'red',
}
