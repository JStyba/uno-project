import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { TwoChildComponent } from './two/two-child/two-child.component';
import { ThreeComponent } from './three/three.component';
import { FourComponent } from './four/four.component';
import { FiveComponent } from './five/five.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ColorChangeDirective } from './color-change.directive';
import { ColorChangeHoverDirective } from './color-change-hover.directive';
import { ModuloPipe } from './modulo.pipe'

@NgModule({
  declarations: [
    AppComponent,
    OneComponent,
    TwoComponent,
    TwoChildComponent,
    ThreeComponent,
    FourComponent,
    FiveComponent,
    ColorChangeDirective,
    ColorChangeHoverDirective,
    ModuloPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    AccordionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
