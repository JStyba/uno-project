import { AfterContentChecked, AfterContentInit, AfterViewChecked, Component, DoCheck, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';

@Component({
  selector: 'sda-two-child',
  templateUrl: './two-child.component.html',
  styleUrls: ['./two-child.component.scss']
})
export class TwoChildComponent implements OnInit, OnChanges, DoCheck,AfterContentInit, AfterContentChecked, AfterViewChecked, OnDestroy {
@Input() dataInput: string = 'default text from child component';
@Output() dataFromChild = new EventEmitter<string | void>(); // there can be a value or not
  constructor() {
    console.log(`constructor, new - dataInput is ${this.dataInput}`);
    // AT THIS POINT dataInput IS NOT READY (NOT INITIALIZED BY PARENT COMPONENT)
  }

  ngOnChanges() {
    console.log(`ngOnChanges - dataInput is ${this.dataInput}`);
  }

  ngOnInit() {
    this.emitEvent(); // this is the place where we emit event
    console.log(`ngOnInit  - dataInput is ${this.dataInput}`);
    // THIS IS THE HOOK WHERE THE DATA INPUT IS READY, AND ALL THE OTHER ELEMENTS ARE READY TO BE USED
  }

  ngDoCheck() {
    console.log('ngDoCheck');
  }

  ngAfterContentInit() {
    console.log('ngAfterContentInit');
  }

  ngAfterContentChecked() {
    console.log('ngAfterContentChecked');
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');
  }

  ngAfterViewChecked() {
    console.log('ngAfterViewChecked');
  }

  ngOnDestroy() {
    console.log('ngOnDestroy');
    console.warn('=============================');
  }

emitEvent() {
  this.dataFromChild.emit('data from child');
}
}
