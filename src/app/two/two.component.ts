import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sda-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.scss']
})
export class TwoComponent implements OnInit {
  selectedTab: boolean = true;
  variableFromParent: string = 'This is a variable from parent';
  constructor() { }

  ngOnInit(): void {
  }

  receiveMessage(text: string | void): void {
    console.log(text);
  }

}
